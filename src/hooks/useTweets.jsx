import { useContext } from "react";
import TweetsContext from "../context/TweetsProvider";

const useTweets = () => {
  return useContext(TweetsContext);
};

export default useTweets;
