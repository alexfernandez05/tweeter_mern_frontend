import { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";

import clienteAxios from "../config/clienteAxios";
import Alerta from "../componentes/Alerta";

const NuevoPassword = () => {
  const [alerta, setAlerta] = useState({});
  const [tokenValido, setTokenValido] = useState(false);
  const [password, setPassword] = useState("");
  const [passwordModificado, setPasswordModificado] = useState(false);

  const params = useParams();
  const { token } = params;

  useEffect(() => {
    const comprobarToken = async () => {
      try {
        await clienteAxios.get(`/usuarios/olvide-password/${token}`);

        setTokenValido(true);
      } catch (error) {
        setAlerta({
          msg: error.response.data.msg,
          error: true,
        });
      }
    };

    comprobarToken();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (password.length < 6) {
      setAlerta({
        msg: "La contraseña debe tener al menos 6 caracteres",
        error: true,
      });
      return;
    }

    try {
      const url = `/usuarios/olvide-password/${token}`;

      const { data } = await clienteAxios.post(url, { password });

      setAlerta({
        msg: data.msg,
        error: false,
      });

      setPasswordModificado(true);
    } catch (error) {
      setAlerta({
        msg: error.response.data.msg,
        error: true,
      });
    }
  };

  const { msg } = alerta;

  return (
    <>
      <div className="flex items-center justify-center p-10">
        <img className="h-20" src="/assets/logo.svg" alt="" />
      </div>

      <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
        <div className="w-full flex flex-col justify-center">
          <h1 className="text-sky-600 font-black text-6xl">
            <p>Restablece tu password</p>
            <span className="text-slate-700">Descubre lo que está pasando</span>
          </h1>
        </div>

        {tokenValido && (
          <form
            className="bg-white shadow rounded-lg p-10 mt-5 md:mt-0"
            onSubmit={handleSubmit}
          >
            {msg && <Alerta alerta={alerta} />}

            <div className="my-5 ">
              <label
                className="uppercase text-gray-600 block text-xl font-bold"
                htmlFor="password"
              >
                Nuevo Password
              </label>
              <input
                id="password"
                type="password"
                placeholder="Escribe tu nuevo Password"
                className="w-full mt-3 p-3 border rounded-xl bg-gray-50"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>

            <input
              type="submit"
              value="Guardar nuevo password"
              className="bg-sky-700 w-full py-3 text-white uppercase font-bold rounded hover:cursor-pointer hover:bg-sky-800 transition-colors"
            />
          </form>
        )}
      </div>

      {passwordModificado && (
        <nav className="p-10">
          <Link
            className="block text-center my-5 text-slate-500 uppercase text-sm"
            to="/"
          >
            Iniciar Sesión
          </Link>
        </nav>
      )}
    </>
  );
};

export default NuevoPassword;
