import { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";

import clienteAxios from "../config/clienteAxios";
import Alerta from "../componentes/Alerta";

const ConfirmarCuenta = () => {
  const [alerta, setAlerta] = useState({});
  const [cuentaConfirmada, setCuentaConfirmada] = useState(false);

  const params = useParams();
  const { id } = params;
  // console.log(id);

  useEffect(() => {
    const confirmarCuenta = async () => {
      try {
        const url = `/usuarios/confirmar/${id}`;
        const { data } = await clienteAxios.get(url);

        setAlerta({
          msg: data.msg,
          error: false,
        });

        setCuentaConfirmada(true);
      } catch (error) {
        setAlerta({
          msg: error.response.data.msg,
          error: true,
        });
      }
    };

    confirmarCuenta();
  }, []);

  const { msg } = alerta;

  return (
    <>
      <div className="flex items-center justify-center p-10">
        <img className="h-20" src="/assets/logo.svg" alt="" />
      </div>

      <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
        <div className="w-full flex flex-col justify-center">
          <h1 className="text-sky-600 font-black text-6xl capitalize">
            <p>Confirma tu cuenta</p>
            <span className="text-slate-700">descubre lo que está pasando</span>
          </h1>
        </div>

        <div className="shadow-lg px-5 py-10 rounded-xl bg-white">
          {msg && <Alerta alerta={alerta} />}

          {cuentaConfirmada && (
            <Link
              className="block text-center mt-10 mb-5 text-slate-500 uppercase text-sm"
              to="/"
            >
              Iniciar Sesión
            </Link>
          )}
        </div>
      </div>
    </>
  );
};

export default ConfirmarCuenta;
