import { useState, useEffect } from "react";

import clienteAxios from "../config/clienteAxios";

import FormularioTweet from "../componentes/FormularioTweet";
import Tweet from "../componentes/Tweet";

import io from "socket.io-client";

let socket;

const Tweets = () => {
  const [listOfTweets, setListOfTweets] = useState([]);

  useEffect(() => {
    socket = io(import.meta.env.VITE_BACKEND_URL);
  });

  useEffect(() => {
    socket.on("broadcast", (data) => {
      console.log(data.description);
    });

    socket.on("tweet-anadido", (tweet) => {
      setListOfTweets([tweet, ...listOfTweets]);
    });

    socket.on("tweet-retweetado", (tweet) => {
      setListOfTweets([tweet, ...listOfTweets]);
    });

    socket.on("tweet-like", (tweet) => {
      console.log([tweet, ...listOfTweets]);
      setListOfTweets([tweet, ...listOfTweets]);
    });

    socket.on("respuesta-nueva", (tweet) => {
      setListOfTweets([tweet, ...listOfTweets]);
    });
  });

  useEffect(() => {
    const obtenerTweetsApi = async (e) => {
      try {
        const token = localStorage.getItem("token");
        let { data } = await clienteAxios.get("/tweet/", {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        });

        data = data.reverse();

        setListOfTweets([...data]);
      } catch (error) {
        console.log("Error obtenint tweets de la base de dades", error);
      }
    };

    obtenerTweetsApi();
  }, []);

  return (
    <div className="mt-5 p-5">
      <FormularioTweet
        listOfTweets={listOfTweets}
        setListOfTweets={setListOfTweets}
      />

      {listOfTweets.length > 0 &&
        listOfTweets.map((tweet) => (
          <Tweet
            key={tweet._id}
            tweet={tweet}
            listOfTweets={listOfTweets}
            setListOfTweets={setListOfTweets}
          />
        ))}
    </div>
  );
};

export default Tweets;
