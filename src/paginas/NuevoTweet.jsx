import FormularioTweet from "../componentes/FormularioTweet";

const NuevoTweet = () => {
  return (
    <>
      <h1 className="text-4xl font-black">Crear Tweet</h1>

      <div className="mt-10 flex justify-center">
        <FormularioTweet />
      </div>
    </>
  );
};

export default NuevoTweet;
