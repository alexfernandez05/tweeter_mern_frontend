import { useState, useEffect } from "react";

import clienteAxios from "../config/clienteAxios";

import Tweet from "../componentes/Tweet";

import useAuth from "../hooks/useAuth";

const Perfil = () => {
  const [userTweets, setUserTweets] = useState([]);

  const { auth } = useAuth();

  useEffect(() => {
    const obtenerTweetsApi = async (e) => {
      try {
        const token = localStorage.getItem("token");
        let { data } = await clienteAxios.get("/tweet/", {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        });

        // data = data.reverse();
        data = data.filter((tweet) => tweet.usuario._id === auth._id).reverse();

        console.log(data);

        setUserTweets([...data]);
      } catch (error) {
        console.log("Error obtenint tweets de la base de dades", error);
      }
    };

    obtenerTweetsApi();
  }, []);

  return (
    <>
      <div className="relative ">
        <div className="h-72 bg-[url('https://librosostenibilidad.files.wordpress.com/2017/03/paisaje-cultura-sostenibilidad.jpg')]"></div>

        <div className="bg-white shadow -mt-5 mx-5 rounded-xl p-10 pt-20 flex flex-col items-center ">
          <p className="text-3xl font-bold mb-3">{auth.nombre}</p>
          <p className="text-xl">
            Photographer & Filmmaker based in Copenhagen, Denmark ✵ 🇩🇰
          </p>
          <div className="flex p-5 gap-5">
            <div>
              <span>2.569</span>
              <span className="ml-2 text-slate-500">Seguidos</span>
            </div>
            <div>
              <span>2.569</span>
              <span className="ml-2 text-slate-500">Seguidores</span>
            </div>
          </div>
          <button className="flex gap-3 bg-blue-500 hover:bg-blue-700 text-white py-2 px-7 rounded">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M18 9v3m0 0v3m0-3h3m-3 0h-3m-2-5a4 4 0 11-8 0 4 4 0 018 0zM3 20a6 6 0 0112 0v1H3v-1z"
              />
            </svg>
            Seguir
          </button>

          <div className="bg-white absolute left-50 top-44 p-1 rounded-md">
            <img
              src="https://xavierferras.com/wp-content/uploads/2019/02/266-Persona.jpg"
              alt=""
              className="h-36 rounded-md"
            />
          </div>
        </div>
      </div>

      <div className="p-5 flex flex-col md:flex-row gap-5">
        <div className="md:w-1/3 bg-white shadow py-5 md:mt-10 rounded-md">
          <p className="px-10 py-2 text-blue-500 font-bold border-l-4 border-blue-500">
            Tweets
          </p>
          <p className="px-10 py-2 border-l-4 border-white">
            Tweets y respuestas
          </p>
          <p className="px-10 py-2 border-l-4 border-white">Likes</p>
          <p className="px-10 py-2 border-l-4 border-white">Guardados</p>
        </div>

        <div className="w-full shadow md:mt-10 mb-16">
          {userTweets.length > 0 ? (
            <>
              {userTweets.map((tweet) => (
                <Tweet key={tweet._id} tweet={tweet} />
              ))}
            </>
          ) : (
            <div className="bg-white p-5 text-center rounded-md">
              <p>Todavía no has publicado tweets</p>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Perfil;
