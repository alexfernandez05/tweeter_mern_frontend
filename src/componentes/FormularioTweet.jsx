import { useState, useEffect } from "react";

import Axios from "axios";

import clienteAxios from "../config/clienteAxios";
import useAuth from "../hooks/useAuth";

import io from "socket.io-client";

let socket;

const FormularioTweet = (props) => {
  const [contenido, setContenido] = useState("");
  const [imagen, setImagen] = useState("");

  useEffect(() => {
    socket = io(import.meta.env.VITE_BACKEND_URL);
  });

  const uploadImage = async (e) => {
    // Guardar imagen el cloudinary
    if (e.target.files[0]) {
      const formData = new FormData();
      formData.append("file", e.target.files[0]);
      formData.append("upload_preset", "tweeter_uploads");

      Axios.post(
        "https://api.cloudinary.com/v1_1/dt4mofcrj/image/upload",
        formData
      ).then((res) => {
        setImagen(res.data.url);
      });
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      // Truquito para que esperemos a la respuesta de cloudinary
      setTimeout(async () => {
        // Guardar en la base de datos
        const token = localStorage.getItem("token");

        const { data } = await clienteAxios.post(
          "/tweet/add",
          {
            contenido,
            imagen,
          },
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
          }
        );

        //Activate the setListOfTweets to rerender the main layout
        // props.setListOfTweets([data, ...props.listOfTweets]);

        setContenido("");
        setImagen("");

        // Socket.io
        socket.emit("nuevo-tweet", data);
      }, 3000);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="p-5 mb-6 bg-white shadow rounded-lg flex flex-col">
      <div className="flex flex-row border-b-1 pb-2 mb-2">
        <p className="text-base font-semibold">Tweet algo</p>
      </div>

      <div className="flex flex-row">
        <div>
          <img
            src="https://pbs.twimg.com/profile_images/898280220037443585/mthp5TlW_400x400.jpg"
            alt="logoUser"
            className="rounded-md h-12 w-12"
          />
        </div>
        <div className="w-full">
          <form onSubmit={handleSubmit}>
            <textarea
              className="form-control mb-3 block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding m-0 resize-none
                      focus:text-gray-700 focus:text-base focus:bg-white focus:outline-none"
              id="exampleFormControlTextarea1"
              rows="3"
              placeholder="¿Qué esta pasando?"
              value={contenido}
              onChange={(e) => setContenido(e.target.value)}
            ></textarea>

            <div className="grid grid-cols-2 pl-3">
              <label htmlFor="uploadFile" className="self-end cursor-pointer">
                <svg
                  className="self-end h-5 w-5 text-blue-500"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                >
                  <rect x="3" y="3" width="18" height="18" rx="2" ry="2" />
                  <circle cx="8.5" cy="8.5" r="1.5" />
                  <polyline points="21 15 16 10 5 21" />
                </svg>
              </label>

              <input
                type="file"
                name="uploadFile"
                id="uploadFile"
                className="hidden"
                onChange={uploadImage}
              />
              <button className="justify-self-end bg-blue-500 hover:bg-blue-700 text-white py-2 px-7 rounded">
                Tweet
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default FormularioTweet;
