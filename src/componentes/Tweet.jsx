import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import useAuth from "../hooks/useAuth";

import clienteAxios from "../config/clienteAxios";

import io from "socket.io-client";

let socket;

const Tweet = (props) => {
  const [comentario, setComentario] = useState("");
  const [teHaGustado, setTeHaGustado] = useState(false);
  const [loHasGuardado, setLoHasGuardado] = useState(false);
  const [loHasRetweeteado, setLoHasRetweeteado] = useState(false);

  const { auth } = useAuth();

  const tweet = props.tweet;
  const token = localStorage.getItem("token");

  const options = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  };

  useEffect(() => {
    socket = io(import.meta.env.VITE_BACKEND_URL);
  });

  const miLike = (tweet.likes || []).filter(
    (like) => like.usuario._id == auth._id
  );

  const miGuardado = (tweet.guardados || []).filter(
    (guardado) => guardado.usuario._id == auth._id
  );

  const miRetweet = (tweet.retweets || []).filter(
    (retweet) => retweet.usuario._id == auth._id
  );

  //Populating the like, retweet, saved buttons
  useEffect(() => {
    if (miLike.length) {
      setTeHaGustado(miLike[0].meHaGustado);
    }
    if (miGuardado.length) {
      setLoHasGuardado(miGuardado[0].loHeGuardado);
    }
    if (miRetweet.length) {
      setLoHasRetweeteado(miRetweet[0].loHeRetweeteado);
    }
  }, []);

  //Logic click like button
  const likeClick = async (e) => {
    try {
      if (!miLike.length || (miLike.length && !miLike[0].meHaGustado)) {
        let like = {
          meHaGustado: true,
          usuario: auth,
        };

        tweet.likes.push(like);

        //Guardar tweet en la lista de tweets gustados de usuario
        auth.tweetsGustados.push(tweet._id);
      } else {
        tweet.likes = tweet.likes.filter(
          (like) => like.usuario._id != auth._id
        );

        auth.tweetsGustados = auth.tweetsGustados.filter(
          (tweetGustado) => tweetGustado != tweet._id
        );
      }

      setTeHaGustado((current) => !current);

      const { data } = await clienteAxios.put(
        `/tweet/edit/${tweet._id}`,
        {
          tweet,
        },
        options
      );

      //Guardar usuario
      const { dataUsuario } = await clienteAxios.put(
        `/usuarios/edit/${auth._id}`,
        {
          auth,
        },
        options
      );

      // Socket.io
      socket.emit("nuevo-like", data);
    } catch (error) {
      console.log(error);
    }
  };

  //Logic click save button
  const guardadoClick = async (e) => {
    try {
      if (
        !miGuardado.length ||
        (miGuardado.length && !miGuardado[0].loHeGuardado)
      ) {
        let guardado = {
          loHeGuardado: true,
          usuario: auth,
        };

        tweet.guardados.push(guardado);

        //Guardar tweet en la lista de tweets guardados de usuario
        auth.tweetsGuardados.push(tweet._id);
      } else {
        tweet.guardados = tweet.guardados.filter(
          (guardado) => guardado.usuario._id != auth._id
        );

        auth.tweetsGuardados = auth.tweetsGuardados.filter(
          (tweetGuardado) => tweetGuardado != tweet._id
        );
      }

      setLoHasGuardado((current) => !current);

      const { data } = await clienteAxios.put(
        `/tweet/edit/${tweet._id}`,
        {
          tweet,
        },
        options
      );

      //Guardar usuario
      const { dataUsuario } = await clienteAxios.put(
        `/usuarios/edit/${auth._id}`,
        {
          auth,
        },
        options
      );

      // Socket.io
      socket.emit("nuevo-guardado", data);
    } catch (error) {
      console.log(error);
    }
  };

  //Logic click retweet button
  const retweetClick = async (e) => {
    try {
      if (
        !miRetweet.length ||
        (miRetweet.length && !miRetweet[0].loHeRetweeteado)
      ) {
        let retweet = {
          loHeRetweeteado: true,
          usuario: auth,
        };

        //Logica retweet que sea nuevo tweet
        let newRetweet = JSON.parse(JSON.stringify(tweet));
        newRetweet.esRetweet = {
          nombrePersona: auth.nombre,
          propietario: tweet.usuario.nombre,
        };

        const { data } = await clienteAxios.post(
          "/tweet/add",
          {
            contenido: newRetweet.contenido,
            imagen: newRetweet.imagen,
            esRetweet: newRetweet.esRetweet,
          },
          options
        );

        // Socket.io
        socket.emit("nuevo-retweet", data);

        //Activate the setListOfTweets to rerender the main layout
        props.setListOfTweets([data, ...props.listOfTweets]);

        tweet.retweets.push(retweet);
      } else {
        tweet.retweets = tweet.retweets.filter(
          (retweet) => retweet.usuario._id != auth._id
        );
      }

      setLoHasRetweeteado((current) => !current);

      const { data } = await clienteAxios.put(
        `/tweet/edit/${tweet._id}`,
        {
          tweet,
        },
        options
      );
    } catch (error) {
      console.log(error);
    }
  };

  const enviarRespuesta = async (e) => {
    try {
      if (e.key === "Enter") {
        // console.log(tweet);
        let objComentario = {
          propietarioComentario: auth.nombre,
          fechaComentario: new Date(),
          contenidoComentario: comentario,
        };

        tweet.listaComentarios.push(objComentario);

        const { data } = await clienteAxios.put(
          `/tweet/edit/${tweet._id}`,
          {
            tweet,
          },
          options
        );

        setComentario("");

        // Socket.io
        socket.emit("nueva-respuesta", data);
      }
    } catch (error) {
      console.log("Error enviando respuesta ", error);
    }
  };

  return (
    <div>
      {tweet.esRetweet ? (
        <div>
          <svg
            className="h-5 w-5 my-2 mr-2 text-gray-500 inline-block"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            {" "}
            <path stroke="none" d="M0 0h24v24H0z" />{" "}
            <path d="M20 11a8.1 8.1 0 0 0 -15.5 -2m-.5 -5v5h5" />{" "}
            <path d="M4 13a8.1 8.1 0 0 0 15.5 2m.5 5v-5h-5" />
          </svg>
          <label className="text-gray-500 inline-block">
            {tweet.esRetweet.nombrePersona} Retweeted
          </label>
        </div>
      ) : (
        ""
      )}
      <div className="p-5 mb-6 bg-white shadow rounded-md flex flex-col">
        <div className="flex flex-row pb-6">
          <div className="pr-4">
            <img
              src="https://pbs.twimg.com/profile_images/898280220037443585/mthp5TlW_400x400.jpg"
              alt="logoUser"
              className="rounded-md h-12 w-12"
            />
          </div>

          <div className="grid place-content-end">
            <p className="text-lg font-semibold">
              {tweet.esRetweet
                ? tweet.esRetweet.propietario
                : tweet.usuario.nombre}
            </p>
            <p className="text-xs text-gray-400">
              {new Intl.DateTimeFormat("es-ES", {
                year: "numeric",
                month: "long",
                day: "2-digit",
              }).format(Date.parse(tweet.fechaPublicacion))}
            </p>
          </div>
        </div>

        <div className="flex flex-col">
          <div>
            <p className="pb-5">{tweet.contenido}</p>
          </div>
          <div className="flex justify-center">
            {tweet.imagen ? (
              <img src={tweet.imagen} alt="photo" className="rounded-md" />
            ) : (
              ""
            )}
          </div>
        </div>

        <div className="flex flex-row justify-end pt-2">
          <p className="text-xs text-gray-400 mr-3">
            {tweet.listaComentarios.length} Comments
          </p>
          <p className="text-xs text-gray-400 mr-3">
            {tweet.retweets.length} Retweets
          </p>
          <p className="text-xs text-gray-400">{tweet.likes.length} Likes</p>
        </div>

        <div className="flex flex-row justify-around border-y-1 my-2 p-2">
          <div>
            <button className="bg-transparent hover:bg-gray-100 md:py-2 px-4 hover:border-transparent rounded">
              <svg
                className="h-5 w-5 md:h-7 md:w-7 sm:mr-4 inline-block"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z" />
              </svg>
              <span className="hidden lg:inline-block">Comment</span>
            </button>
          </div>

          <div>
            <button
              className="bg-transparent hover:bg-gray-100 md:py-2 px-4 hover:border-transparent rounded"
              onClick={(e) => retweetClick(e.target.value)}
            >
              <svg
                className={`h-5 w-5 md:h-7 md:w-7 sm:mr-4 inline-block ${
                  loHasRetweeteado ? "text-green-500" : "text-black"
                }`}
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"
                />
              </svg>
              <span
                className={`hidden lg:inline-block font-medium ${
                  loHasRetweeteado ? "text-green-500" : "text-black"
                }`}
              >
                {loHasRetweeteado ? "Retweeted" : "Retweet"}
              </span>
            </button>
          </div>

          <div>
            <button
              className="bg-transparent hover:bg-gray-100 md:py-2 px-4 hover:border-transparent rounded"
              onClick={(e) => likeClick(e.target.value)}
            >
              <svg
                className={`h-5 w-5 md:h-7 md:w-7 sm:mr-4 inline-block ${
                  teHaGustado ? "text-red-500" : "text-black"
                }`}
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
                />
              </svg>
              <span
                className={`hidden lg:inline-block font-medium ${
                  teHaGustado ? "text-red-500" : "text-black"
                }`}
              >
                {teHaGustado ? "Liked" : "Like"}
              </span>
            </button>
          </div>
          <div>
            <button
              className="bg-transparent hover:bg-gray-100 md:py-2 px-4 hover:border-transparent rounded"
              onClick={(e) => guardadoClick(e.target.value)}
            >
              <svg
                className={`h-5 w-5 md:h-7 md:w-7 sm:mr-4 inline-block ${
                  loHasGuardado ? "text-blue-500" : "text-black"
                }`}
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z" />
              </svg>
              <span
                className={`hidden lg:inline-block font-medium ${
                  loHasGuardado ? "text-blue-500" : "text-black"
                }`}
              >
                {loHasGuardado ? "Saved" : "Save"}
              </span>
            </button>
          </div>
        </div>
        <div className="flex flex-row gap-2 pb-2 border-b-1">
          <div className="">
            <img
              src="https://pbs.twimg.com/profile_images/898280220037443585/mthp5TlW_400x400.jpg"
              alt="logoUser"
              className="rounded-md h-12 w-12"
            />
          </div>
          <div className="w-full">
            <input
              className="appearance-none border rounded w-full h-12 py-2 px-3 text-gray-700 leading-tight focus:outline-none bg-slate-50"
              id="tweetReply"
              type="text"
              placeholder="Tweet your reply"
              value={comentario}
              onChange={(e) => setComentario(e.target.value)}
              onKeyDown={enviarRespuesta}
            />
          </div>
        </div>

        {tweet.listaComentarios.length
          ? tweet.listaComentarios.map((comentario) => (
              <div
                className="p-2 mt-3 bg-slate-100 rounded-md"
                key={`${comentario.propietarioComentario}_${comentario.contenidoComentario}`}
              >
                <div className="flex flex-row">
                  <div className="pr-4">
                    <img
                      src="https://pbs.twimg.com/profile_images/898280220037443585/mthp5TlW_400x400.jpg"
                      alt="logoUser"
                      className="rounded-md h-12 w-12"
                    />
                  </div>

                  <div className="flex items-center">
                    <div className="block ">
                      <label className="text-lg font-semibold inline pr-4">
                        {comentario.propietarioComentario}
                      </label>
                      <br className="block sm:hidden" />
                      <label className="text-xs text-gray-400 inline">
                        {new Intl.DateTimeFormat("es-ES", {
                          year: "numeric",
                          month: "long",
                          day: "2-digit",
                        }).format(Date.parse(comentario.fechaComentario))}
                      </label>
                      <br />
                      <p className="inline">{comentario.contenidoComentario}</p>
                    </div>
                  </div>
                </div>
              </div>
            ))
          : ""}
      </div>
    </div>
  );
};

export default Tweet;
