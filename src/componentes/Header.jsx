import { Link } from "react-router-dom";

import useAuth from "../hooks/useAuth";

import Dropdown from "../componentes/Dropdown";

const Header = () => {
  const { auth } = useAuth();

  return (
    <header className="px-4 py-5 bg-white border-b">
      <div className="flex items-center place-content-between">
        <div>
          <Link to="/tweets">
            <img className="h-10" src="/assets/logo.svg" alt="" />
          </Link>
        </div>

        <div className="hidden md:flex">
          <div className="flex p-5 items-center place-content-between">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-7 w-7 mb-1"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
              />
            </svg>
            <p className=" border-sky-600">Inicio</p>
          </div>

          <div className="flex p-5 items-center place-content-between">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-7 w-7 mb-1"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
              />
            </svg>
            <p className="">Explorar</p>
          </div>

          <div className="flex p-5 items-center place-content-between">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-7 w-7 mb-1"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
              />
            </svg>
            <p className="">Notificaciones</p>
          </div>
        </div>

        <Dropdown />
      </div>
    </header>
  );
};

export default Header;
