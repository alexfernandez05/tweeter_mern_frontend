const BottomNavigation = () => {
  return (
    <section
      id="bottom-navigation"
      className="block fixed inset-x-0 bottom-0 z-10 bg-white shadow"
    >
      <div className="flex justify-between p-2">
        <a
          href="#"
          className="w-full focus:text-sky-600 hover:text-sky-600 
                flex flex-col items-center justify-center text-center pt-2 pb-1"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-7 w-7 mb-1"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            strokeWidth={2}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
            />
          </svg>
          {/* <span class="tab tab-home block text-xs">Inicio</span> */}
        </a>

        <a
          href="#"
          className="w-full focus:text-sky-600 hover:text-sky-600 
                flex flex-col items-center justify-center text-center pt-2 pb-1"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-7 w-7 mb-1"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            strokeWidth={2}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
            />
          </svg>
          {/* <span class="tab tab-home block text-xs">Explorar</span> */}
        </a>

        <a
          href="#"
          className="w-full focus:text-sky-600 hover:text-sky-600 
                flex flex-col items-center justify-center text-center pt-2 pb-1"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-7 w-7 mb-1"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            strokeWidth={2}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
            />
          </svg>
          {/* <span class="tab tab-home block text-xs">Notificaciones</span> */}
        </a>
      </div>
    </section>
  );
};

export default BottomNavigation;
