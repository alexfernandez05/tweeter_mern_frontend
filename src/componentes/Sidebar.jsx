import { Link } from "react-router-dom";
import useAuth from "../hooks/useAuth";

const Sidebar = ({ show = false }) => {
  const { auth } = useAuth();

  return show && (
    <aside className="hidden lg:block md:w-44 2xl:w-96 px-5 py-10">
    </aside>
  );
};

export default Sidebar;
