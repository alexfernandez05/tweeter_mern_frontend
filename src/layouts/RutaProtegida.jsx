import { Outlet, Navigate, useLocation } from "react-router-dom";

import useAuth from "../hooks/useAuth";

import Header from "../componentes/Header";
import Sidebar from "../componentes/Sidebar";
import BottomNavigation from "../componentes/BottomNavigation";
import Spinner from "../componentes/Spinner";

const RutaProtegida = () => {
  const { auth, cargando } = useAuth();

  const location = useLocation();

  // if (cargando) return "Cargando...";
  if (cargando) return <div className="h-100 w-100 flex items-center justify-center"><Spinner /></div>;

  return (
    <>
      {auth._id ? (
        <div className="bg-gray-100">
          <Header />

          <div className="md:flex md:min-h-screen">
            <Sidebar show={location.pathname === "/tweets"} />

            <main className="flex-1">
              <Outlet />
            </main>

            <Sidebar show={location.pathname === "/tweets"} />
          </div>

          <div className="block md:hidden">
            <BottomNavigation />
          </div>
        </div>
      ) : (
        <Navigate to="/" />
      )}
    </>
  );
};

export default RutaProtegida;
