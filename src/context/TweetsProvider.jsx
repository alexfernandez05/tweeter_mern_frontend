import { useState, useEffect, createContext } from "react";
import clienteAxios from "../config/clienteAxios";
import { useNavigate } from "react-router-dom";

const TweetsContext = createContext();

const TweetsProvider = ({ children }) => {
  const [tweets, setTweets] = useState([]);
  const [alerta, setAlerta] = useState([]);

  const navigate = useNavigate();

  const mostrarAlerta = (alerta) => {
    setAlerta(alerta);

    setTimeout(() => {
      setAlerta({});
    }, 5000);
  };

  const submitTweet = async (tweet) => {
    try {
      const token = localStorage.getItem("token");

      if (!token) return;

      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      };

      const { data } = await clienteAxios.post(`/tweets`, tweet, config);

      setAlerta({
        msg: "Tweet creado correctamente",
        error: false,
      });
      setTimeout(() => {
        setAlerta({});
        navigate("/tweets");
      }, 3000);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <TweetsContext.Provider
      value={{
        tweets,
        mostrarAlerta,
        alerta,
        submitTweet,
      }}
    >
      {children}
    </TweetsContext.Provider>
  );
};

export { TweetsProvider };

export default TweetsContext;
