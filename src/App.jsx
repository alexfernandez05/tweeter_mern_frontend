import { BrowserRouter, Routes, Route } from "react-router-dom";

import AuthLayout from "./layouts/AuthLayout";
import RutaProtegida from "./layouts/RutaProtegida";

import Login from "./paginas/Login";
import Registrar from "./paginas/Registrar";
import OlvidePassword from "./paginas/OlvidePassword";
import NuevoPassword from "./paginas/NuevoPassword";
import ConfirmarCuenta from "./paginas/ConfirmarCuenta";
import Tweets from "./paginas/Tweets";
import NuevoTweet from "./paginas/NuevoTweet";
import Perfil from "./paginas/Perfil";

import { AuthProvider } from "./context/AuthProvider";
import { TweetsProvider } from "./context/TweetsProvider";

function App() {
  return (
    <BrowserRouter>
      <AuthProvider>
        <TweetsProvider>
          <Routes>
            <Route path="/" element={<AuthLayout />}>
              <Route index element={<Login />} />
              <Route path="registrar" element={<Registrar />} />
              <Route path="olvide-password" element={<OlvidePassword />} />
              <Route
                path="olvide-password/:token"
                element={<NuevoPassword />}
              />
              <Route path="confirmar/:id" element={<ConfirmarCuenta />} />
            </Route>

            <Route path="/tweets" element={<RutaProtegida />}>
              <Route index element={<Tweets />} />
              <Route path="crear-tweet" element={<NuevoTweet />} />
            </Route>

            <Route path="/perfil" element={<RutaProtegida />}>
              <Route index element={<Perfil />} />
            </Route>
          </Routes>
        </TweetsProvider>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
